﻿#region HeaderComments
// ***********************************************************************************************************************************************
// Copyright (C) # YEAR # # York #
// 作    者：# CaiTao #
// 创建日期：# CREATIONDATE #
// 功能描述：# 文本文件读取和处理 #
// 修改记录：# MODIFICATIONRECORD #
// ***********************************************************************************************************************************************
#endregion

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;

namespace York.JSONConfig
{
    /// <summary>
    /// 文件类型
    /// </summary>
    public enum FileType
    {
        txt,
        config,
        json,
    }
    public class LoadDocumentInfo
    {
        private static string assetFloder = Application.streamingAssetsPath;
        
        /// <summary>
        /// 加载某个文件夹中的某种类型的所有文件的类型
        /// </summary>
        /// <param name="folderPath">相对路径</param>
        public static Dictionary<string, List<string>> LoadAllFileContent(string folderPath, FileType fileType)
        {
            Dictionary<string, List<string>> pointDic = new Dictionary<string, List<string>>();

            List<string> allFileName = ReadAllFileName(folderPath, fileType);  //读取文件名

            for (int i = 0; i < allFileName.Count; i++)
            {
                string content = ReadText(allFileName[i], fileType, folderPath);

                List<string> list = TextProcessing(content, ';');

                pointDic.Add(allFileName[i], list);
            }

            return pointDic;
        }

        /// <summary>
        /// 获取指定文件夹下的所有后缀为txt文件的名称
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static List<string> ReadAllFileName(string folderRelativePath, FileType fileType)
        {
            List<string> fileNameList = new List<string>();

            string path = Path.Combine(assetFloder, folderRelativePath);

            //获取指定路径下面的所有资源文件  
            if (Directory.Exists(path))
            {
                DirectoryInfo direction = new DirectoryInfo(path);

                FileInfo[] files = direction.GetFiles("*", SearchOption.AllDirectories);

                string pattern = string.Format(".+(?=\\.{0})", fileType.ToString());

                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name.EndsWith("." + fileType.ToString()))
                    {
                        Match fileName = Regex.Match(files[i].Name, pattern);

                        if (!fileNameList.Contains(fileName.Value))
                        {
                            fileNameList.Add(fileName.Value);

                            //Debug.Log(fileName.Value);
                        }
                    }
                }
            }
            else
                Debug.Log("文件夹不存在");
            return fileNameList;
        }

        /// <summary>
        /// 对读取到的文件字符串进行处理成数组
        /// </summary>
        /// <param name="contentStr">需要处理的字符串</param>
        /// <param name="split">分隔符</param>
        /// <returns></returns>
        public static List<string> TextProcessing(string contentStr, char split)
        {
            string[] strArray = contentStr.TrimEnd().TrimEnd(split).Split(split);

            List<string> tempList = new List<string>(strArray);

            //去除空值或null值
            for (int i = 0; i < tempList.Count; i++)
            {
                if (string.IsNullOrEmpty(tempList[i]))
                {
                    tempList.Remove(tempList[i]);
                }
            }
            return tempList;
        }

        /// <summary>
        /// 读取文件中的内容
        /// </summary>
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static string ReadText(string name, FileType fileType, string floderPath = "")
        {
            floderPath = floderPath == "" ? "" : "/" + floderPath;

            string path = Path.Combine(assetFloder + floderPath, name + "." + fileType.ToString());

            if (File.Exists(path))
            {
                string readContentStr = System.IO.File.ReadAllText(path);
                return readContentStr;
            }
            else
            {
                Debug.Log("文件不存在");
                return null;
            }

        }
    }
}

