﻿#region HeaderComments
// ***********************************************************************************************************************************************
// Copyright (C) # YEAR # # York #
// 作    者：# CaiTao #
// 创建日期：# CREATIONDATE #
// 功能描述：# JToken与常用类型转换 #
// 修改记录：# MODIFICATIONRECORD #
// ***********************************************************************************************************************************************
#endregion

using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace York.JSONConfig
{
    public class JsonResolve
    {
        #region Utility
        /// <summary>
        /// 解析三维向量
        /// </summary>
        /// <param name="jObj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Vector3 GetVector3(JObject jObj, string propertyName)
        {
            JObject jv3 = (JObject)jObj[propertyName];

            Vector3 v3 = AnalyzeVector3(jv3);
            return v3;
        }

        /// <summary>
        /// 解析四维向量
        /// </summary>
        /// <param name="jObj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Quaternion GetQuaternion(JObject jObj, string propertyName) {
            JObject jq4 = (JObject)jObj[propertyName];

            Quaternion q4 = AnalyzeQuaternion(jq4);
            return q4;
        }


        /// <summary>
        /// 解析数字
        /// </summary>
        /// <param name="jObj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static float GetFloat(JObject jObj, string propertyName)
        {
            float value = jObj[propertyName].Value<float>();
            return value;
        }

        /// <summary>
        /// 解析三维数据
        /// </summary>
        /// <param name="vector3"></param>
        /// <returns></returns>
        public static Vector3 AnalyzeVector3(JObject vector3)
        {
            float x = ((JValue)vector3["x"]).Value<float>();
            float y = ((JValue)vector3["y"]).Value<float>();
            float z = ((JValue)vector3["z"]).Value<float>();
            Vector3 v3 = new Vector3(x, y, z);
            return v3;
        }


        /// <summary>
        /// 解析四维数据
        /// </summary>
        /// <param name="quaternion"></param>
        /// <returns></returns>
        public static Quaternion AnalyzeQuaternion(JObject quaternion) {
            float x = ((JValue)quaternion["x"]).Value<float>();
            float y = ((JValue)quaternion["y"]).Value<float>();
            float z = ((JValue)quaternion["z"]).Value<float>();
            float w = ((JValue)quaternion["w"]).Value<float>();
            Quaternion qua = new Quaternion(x, y, z,w);
            return qua;
        }

        /// <summary>
        /// 解析数组
        /// </summary>
        /// <param name="jArray"></param>
        /// <returns></returns>
        public static List<string> AnalyzeArray(JArray jArray)
        {
            List<string> array = new List<string>();
            IEnumerator ie = jArray.GetEnumerator();

            while (ie.MoveNext())
            {
                JValue value = (JValue)ie.Current;
                string valStr = value.Value<string>();

                array.Add(valStr);
            }
            return array;
        }

        #endregion
    }
}

